#include "CppProject.hpp"

#include <algorithm>
#include <array>

#include "HeaderFile.hpp"
#include "SourceFile.hpp"

using namespace UAFCppTools;

CppProject::CppProject(const std::filesystem::path &path)
    : mPath(path)
{
    if (!std::filesystem::is_directory(mPath))
    {
        throw std::invalid_argument("Path is not a directory");
    }
}

CppProject::~CppProject()
{

}

std::optional<std::filesystem::path> CppProject::getSourceDirectory() const
{
    std::array<std::string, 2> possibleNames = {
        "src",
        "source"
    };
    for (auto &p: std::filesystem::directory_iterator(mPath))
    {
        if (std::find(possibleNames.begin(),
                      possibleNames.end(),
                      p.path().filename().string()) != possibleNames.end())
        {
            return p;
        }
    }
    return std::nullopt;
}

void CppProject::findSourceFiles()
{
    const auto &sourcePath = this->getSourceDirectory();
    if (!sourcePath) return;

    mSourceFiles.clear();
    for (auto &p: std::filesystem::directory_iterator(sourcePath.value()))
    {
        if (p.is_regular_file())
        {
            auto ext = p.path().extension();
            if (ext == ".hpp")
            {
                mSourceFiles.push_back(std::make_shared<HeaderFile>(p));
            }
            else if (ext == ".cpp")
            {
                mSourceFiles.push_back(std::make_shared<SourceFile>(p));
            }
        }
    }
}

std::vector<std::shared_ptr<CppFile>> CppProject::getSourceFiles()
{
    const auto &sourcePath = this->getSourceDirectory();
    if (!sourcePath) return {};

    // Find and update the internal source files
    findSourceFiles();

    return mSourceFiles;
}
