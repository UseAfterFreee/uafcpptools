#ifndef HEADERFILE_HPP
#define HEADERFILE_HPP

#include <filesystem>

#include "CppFile.hpp"

namespace UAFCppTools
{
    class HeaderFile : public CppFile
    {
        private:
        protected:
        public:
            HeaderFile(std::filesystem::path path);
            ~HeaderFile();
    };
}

#endif
