#include "CppFile.hpp"

#include <filesystem>

using namespace UAFCppTools;

CppFile::CppFile(std::filesystem::path path)
    : mPath(path)
{

}

CppFile::~CppFile()
{

}

std::string CppFile::getFileName() const
{
    return mPath.filename().string();
}
