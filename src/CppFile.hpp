#ifndef CPPFILE_HPP
#define CPPFILE_HPP

#include <filesystem>

#include "UAFCppTools.hpp"

namespace UAFCppTools
{
    class CppFile
    {
        private:
            std::filesystem::path mPath;
        protected:
        public:
            CppFile(std::filesystem::path path);
            virtual ~CppFile();

            std::string getFileName() const;
    };
}

#endif
