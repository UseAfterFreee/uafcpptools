#ifndef CPPPROJECT_HPP
#define CPPPROJECT_HPP

#include <filesystem>
#include <optional>
#include <vector>
#include <memory>

#include "CppFile.hpp"

namespace UAFCppTools
{
    class CppProject
    {
        private:
            std::filesystem::path mPath;
            std::vector<std::shared_ptr<CppFile>> mSourceFiles;

            void findSourceFiles();
        protected:
        public:
            CppProject(const std::filesystem::path &);
            virtual ~CppProject();
            
            std::optional<std::filesystem::path> getSourceDirectory() const;
            std::vector<std::shared_ptr<CppFile>> getSourceFiles();
    };
}

#endif
