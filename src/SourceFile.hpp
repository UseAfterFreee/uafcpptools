#ifndef SOURCEFILE_HPP
#define SOURCEFILE_HPP

#include <filesystem>

#include "CppFile.hpp"

namespace UAFCppTools
{
    class SourceFile : public CppFile
    {
        private:
        protected:
        public:
            SourceFile(std::filesystem::path path);
            ~SourceFile();
    };
}

#endif
