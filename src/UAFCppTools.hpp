#ifndef UAFCPPTOOLS_HPP
#define UAFCPPTOOLS_HPP

#include <memory>

namespace UAFCppTools
{
    class CppProject; // forward declare

    using ProjectPtr = std::shared_ptr<CppProject>;
    template<typename ... Args>
    constexpr ProjectPtr createProject(Args&& ... args)
    {
        return std::make_shared<CppProject>(std::forward<Args>(args)...);
    }
}

#endif
