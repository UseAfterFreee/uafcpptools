SRC_DIR=src
TEST_DIR=test
BIN_DIR=bin
OBJ_DIR=obj

SRC=$(wildcard $(SRC_DIR)/*.cpp)
OBJ=$(patsubst $(SRC_DIR)/%.cpp, $(OBJ_DIR)/%.o, $(SRC))
TEST_SRC=$(wildcard $(TEST_DIR)/*.cpp)
UNIT_OBJ=$(patsubst $(TEST_DIR)/%.cpp, $(OBJ_DIR)/%.o, $(TEST_SRC))
TEST_OBJ=$(filter-out $(OBJ_DIR)/main.o, $(OBJ)) $(UNIT_OBJ)
RUN_OBJ=$(filter-out $(UNIT_OBJ), $(OBJ))

CFLAGS=-Iexternal -std=c++17

.PHONY: directories debug

all: directories $(BIN_DIR)/test $(BIN_DIR)/run

directories: $(BIN_DIR) $(OBJ_DIR)

$(BIN_DIR):
	mkdir -p $@

$(OBJ_DIR):
	mkdir -p $@

$(OBJ_DIR)/%.o: $(SRC_DIR)/%.cpp
	g++ -g -c -fprofile-arcs -ftest-coverage $(CFLAGS) -o $@ $<

$(OBJ_DIR)/%.o: $(TEST_DIR)/%.cpp
	g++ -g -c -fprofile-arcs -ftest-coverage $(CFLAGS) -o $@ $<

$(BIN_DIR)/test: $(TEST_OBJ)
	g++ -fprofile-arcs -ftest-coverage $(CFLAGS) -o $@ $^ -lgtest -lgtest_main

$(BIN_DIR)/run: $(RUN_OBJ)
	g++ -fprofile-arcs -ftest-coverage $(CFLAGS) -o $@ $^

debug:
	echo $(TEST_SRC)
