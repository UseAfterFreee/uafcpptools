#include <gtest/gtest.h>

#include <filesystem>
#include <string>
#include <fstream>

#include "../src/CppFile.hpp"
#include "../src/CppProject.hpp"
#include "../src/UAFCppTools.hpp"

namespace fs = std::filesystem;
namespace uct = UAFCppTools;

TEST(File_Class, Correct_File_Name)
{
    fs::path mppath = fs::current_path() / "test" / "mock_project";
    fs::create_directories(mppath / "src");

    // create a test file
    std::ofstream ofs(mppath / "src" / "TestFile.cpp");
    ofs.close();

    uct::CppFile myFile(mppath / "src" / "TestFile.cpp");

    EXPECT_EQ("TestFile.cpp", myFile.getFileName());
}
