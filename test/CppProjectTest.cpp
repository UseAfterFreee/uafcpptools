#include <gtest/gtest.h>

#include <filesystem>
#include <string>
#include <fstream>

#include "../src/CppProject.hpp"
#include "../src/HeaderFile.hpp"
#include "../src/SourceFile.hpp"

namespace fs = std::filesystem;
namespace uct = UAFCppTools;

TEST(Project_Class, InvalidDirectory)
{
    fs::path some_file_path = fs::current_path() / "test" / "some_file";

    EXPECT_THROW({
        uct::CppProject invalid(std::string{"Some/Invalid/Dir"});
    }, std::invalid_argument);

    // Creates the file
    std::ofstream ofs(some_file_path);
    ofs.close();

    // invalid argument should be thrown when executing with a file instead
    // of a directory
    EXPECT_THROW({
        uct::CppProject invalid(some_file_path);
    }, std::invalid_argument);

    fs::remove_all(some_file_path);
}

TEST(Project_Class, GetSourceDirectoryExists)
{
    fs::path mppath = fs::current_path() / "test" / "mock_project";

    // Create a couple of directories in my mock project
    fs::create_directories(mppath / "src");
    fs::create_directories(mppath / "obj");
    fs::create_directories(mppath / "bin");
    fs::create_directories(mppath / "test");
    fs::create_directories(mppath / "some_dir");

    uct::CppProject mockProject(mppath);
    auto src = mockProject.getSourceDirectory();
    EXPECT_TRUE(src);
    EXPECT_EQ((mppath / "src").string(), src.value().string());

    fs::remove_all(mppath);
}

TEST(Project_Class, GetSourceDirectoryNotExists)
{
    fs::path mppath = fs::current_path() / "test" / "mock_project";

    // Create a couple of directories in my mock project
    fs::create_directories(mppath / "obj");
    fs::create_directories(mppath / "bin");
    fs::create_directories(mppath / "test");
    fs::create_directories(mppath / "some_dir");

    uct::CppProject mockProject(mppath);
    EXPECT_FALSE(mockProject.getSourceDirectory());

    fs::remove_all(mppath);
}

TEST(Project_Class, GetSourceFiles)
{
    fs::path mppath = fs::current_path() / "test" / "mock_project";

    // Create a couple of directories in my mock project
    fs::create_directories(mppath / "src");

    for (std::string s: {"MyClass.hpp", "MyClass.cpp"})
    {
        // Creates the files
        std::ofstream ofs(mppath / "src" / s);
        ofs.close();
    }

    uct::CppProject mockProject(mppath);
    const auto &files = mockProject.getSourceFiles();
    EXPECT_EQ(2, files.size());
    int foundFiles = 0;
    for (auto ptr: files)
    {
        auto fname = ptr->getFileName();
        if (fname == "MyClass.hpp")
        {
            ++foundFiles;

            EXPECT_NE(nullptr, dynamic_cast<uct::HeaderFile*>(ptr.get()));
        }
        if (fname == "MyClass.cpp")
        {
            ++foundFiles;

            EXPECT_NE(nullptr, dynamic_cast<uct::SourceFile*>(ptr.get()));
        }
    }
    EXPECT_EQ(2, foundFiles);

    fs::remove_all(mppath);
}
